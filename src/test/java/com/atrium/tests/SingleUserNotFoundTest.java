package com.atrium.tests;

import com.atrium.TestBase;
import com.atrium.constants.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class SingleUserNotFoundTest extends TestBase {

    @Test
    public void testSingleUserNotFound(){
        given().contentType(ContentType.JSON)
                .when().get(EndPoints.SINGLE_USER_NOT_FOUND_ENDPOINT)
                .then().statusCode(404);
    }
}
